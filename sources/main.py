# Imports
import cocobot
import logging.handlers
import os

# Set logging Level
logging.basicConfig(level=logging.INFO)

# Set logging Handler
handler = logging.FileHandler(
    filename="../logs/bot.log",
    mode="a")
formatter = logging.Formatter("%(asctime)s %(name)-30s %(levelname)-8s %(message)s")
handler.setFormatter(formatter)
logging.getLogger().addHandler(handler)

# Initialise logger
logger = logging.getLogger("main")

# Set working directory
os.chdir(os.path.dirname(os.path.abspath(__file__)))
logger.info(f"Changed working directory to {os.path.dirname(os.path.abspath(__file__))}")

# Initialise and start the Bot
logger.info("Initialising and starting bot")
client = cocobot.Bot()
# noinspection SpellCheckingInspection
# TODO test client.run("NzY4ODA1MjE2NTI2NTMyNjA4.X5Fzpw._VUpCbagvS0k1qy4YRpJ_R69BrY")
client.run("NzcyMDgxMzUxMTM3NTU4NTI4.X51eyg.bPj3Ff_uA8ccXg0yPCOU5nxLT6k")
