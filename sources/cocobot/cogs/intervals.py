import json
import logging

from discord.ext import commands


class Intervals(commands.Cog):
    """
    Groups commands related to the intervals in which the bot joins.

    Note, that all updates to the intervals only apply after the next time the bot joined a channel on your server
    """

    def __init__(self, bot):
        commands.Cog.__init__(self)

        # Initialises the logger
        self.logger = logging.getLogger("cocobot.cogs.intervals")

        # Adds reference to the bit
        self.bot = bot

    @commands.command()
    async def set_min_interval(self, ctx, arg=None):
        """
        Sets the min random interval
        :param ctx: The commands context
        :param arg: The given argument
        """

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"set_min_interval called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                              f"with arg = \"{arg}\"")

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"set_min_interval called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"called outside Bot Channel")
            return

        # If not used correctly, tells the user how to use it
        if arg is None:
            await ctx.send("Usage: `$coco set_min_interval <Time in Minutes>`")
            return
        try:
            arg = int(arg)
        except ValueError:
            await ctx.send("Usage: `$coco set_min_interval <Time in Minutes>`")
            return

        # Tells the user to use a positive number if they gave a negative one
        if arg < 0:
            await ctx.send("The bot cant wait a negative amount of time")
            return

        # Denies non Admins access to this command
        if not ctx.author.guild_permissions.administrator:
            await ctx.send("Only Admins can rchange intervals")
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"set_min_intervals on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                                  f"by non admin")
            return

        # Checks if min interval is less than max interval
        if arg > self.bot.settings[str(ctx.guild.id)]["interval"]["max"]:
            await ctx.send("The min interval cant be bigger than the max interval")
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"set_min_interval called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                                  f"with arg = \"{arg}\" would be bigger than max interval")
            return

        # Sets the min interval
        self.bot.settings[str(ctx.guild.id)]["interval"]["min"] = arg
        with open("../resources/server_settings.json", "w") as settings:
            json.dump(self.bot.settings, settings, indent=4)
        await ctx.send(f"Min interval set to {arg}")
        self.logger.info(f"set min interval to {arg} "
                         f"for (\"{ctx.guild.name}\" = <{ctx.guild.id}>)")

    @commands.command()
    async def set_max_interval(self, ctx, arg=None):
        """
        Sets the max random interval
        :param ctx: The commands context
        :param arg: The given argument
        """

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"set_max_interval on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                              f"with arg = \"{arg}\"")

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"set_max_interval on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"called outside Bot Channel")
            return

        # If not used correctly, tells the user how to use it
        if arg is None:
            await ctx.send("Usage: `$coco set_max_interval <Time in Minutes>`")
            return
        try:
            arg = int(arg)
        except ValueError:
            await ctx.send("Usage: `$coco set_max_interval <Time in Minutes>`")
            return

        # Tells the user to use a positive number if they gave a negative one
        if arg < 0:
            await ctx.send("The bot cant wait a negative amount of time")
            return

        # Denies non Admins access to this command
        if not ctx.author.guild_permissions.administrator:
            await ctx.send("Only Admins can rchange intervals")
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"set_max_interval on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                                  f"by non admin")
            return

        # Checks if max interval is more than min interval
        if arg < self.bot.settings[str(ctx.guild.id)]["interval"]["min"]:
            await ctx.send("The max interval cant be smaller than the min interval")
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"set_max_interval called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                                  f"with arg = \"{arg}\" would be bigger than min interval")
            return

        # Sets the max interval
        self.bot.settings[str(ctx.guild.id)]["interval"]["max"] = arg
        with open("../resources/server_settings.json", "w") as settings:
            json.dump(self.bot.settings, settings, indent=4)
        await ctx.send(f"Max interval set to {arg}")
        self.logger.info(f"set max interval to {arg} "
                         f"for (\"{ctx.guild.name}\" = <{ctx.guild.id}>)")

    @commands.command()
    async def show_interval(self, ctx):
        """
        Shows the user the current interval settings
        :param ctx: The commands Context
        """

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"show_interval called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>)")

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"show_interval on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"called outside Bot Channel")
            return

        # Shows the user the current interval settings
        min_time = self.bot.settings[str(ctx.guild.id)]["interval"]["min"]
        max_time = self.bot.settings[str(ctx.guild.id)]["interval"]["max"]
        await ctx.send(f"Current settings:\n"
                       f"Minimal time: {min_time} Minutes \n"
                       f"Maximal time: {max_time} Minutes")
        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"Sent min {min_time} and max {max_time} "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                              f"on (\"{ctx.guild.name}\" = <{ctx.guild.id}>)")
