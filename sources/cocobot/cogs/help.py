from discord.ext import commands
import logging


class Help(commands.Cog):
    """The cog managing help commands"""

    logger = logging.getLogger("cocobot.cogs.Help")

    def __init__(self, bot):
        commands.Cog.__init__(self)

        # Adds reference to bot
        self.bot = bot

    @commands.command()
    async def help(self, ctx):
        """
        Sends help
        :param ctx: The commands context
        """

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            return

        await ctx.send("This bot is made to join specified channels in random intervals to play a song, then leave"
                       "the channel\n"
                       "An overview of the bots commands (remember: the prefix is $coco) : \n"
                       "```Commands that do something with channels:\n"
                       "    add_channel: Adds a channel to the list of channels the bot can join \n"
                       "    remove_channel: Removes a channel from the list of channels the bot can join\n"
                       "    list_channels: Show the list of channels the bot can join and the IDs the bot assigned "
                       "to the channels\n"
                       "    set_bot_channel: Sets the channel the bot reads messages in. Once set the bot will only"
                       "send messages and react to commands in that channel\n"
                       "Commands that do something with songs:\n"
                       "    add_song: Adds a song to the list of playable songs\n"
                       "    remove_song: Removes a song from the list of playable songs\n"
                       "    list_songs: Shows the list of playable songs and the ID the bot assigned to them\n"
                       "Commands that do something with the intervals in which the bot joins:\n"
                       "  (Any changes made to the intervals only apply after the next time the bot joins a channel)\n"
                       "    set_min_interval: Sets the minimal time the bot waits between joining channels\n"
                       "    set_max_interval: Sets the maximal time the bot waits between joining channels\n"
                       "    show_interval: Shows the current minimal and maximal intervals```"
                       "For more help about a given command just type it in!")
        await ctx.send("Please be aware this bot is very early in developement! If you encounter some weird behaviour"
                       ", have issues, need help or just want to receive updates about the bot please join this server"
                       " https://discord.gg/n9CPXgnanr")
        await ctx.send("This bot was purely a fun project and I dont expecct any money in exchange. Nevertheless, "
                       "if you want to support me and help me pay the bills of the server its hosted on, "
                       "consider buying me a coffe https://www.buymeacoffee.com/Overloader")

    @commands.command()
    async def error(self, ctx, *, arg):
        """
        This command sends an error message to all servers
        :param ctx: The Context of the command
        :param arg: The commands Arguments
        """

        if ctx.author.id == 257231358005477379:
            for guild in self.bot.guilds:
                await self.bot.get_bot_channel(str(guild.id))[1].send(f"A message from the developer:\n{arg}")

    @commands.command()
    async def start(self, ctx):
        """
        Starts the task on the server
        :param ctx: The commands context
        """

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"start called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"called outside Bot Channel")
            return

        # Denies non Admins access to this command
        if not ctx.author.guild_permissions.administrator:
            await ctx.send("Only Admins can start the bot")
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"start on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                                  f"by non admin")
            return

        # If the bot is already running, tell the user
        if ctx.guild.id in self.bot.running:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"Bot already running on (\"{ctx.guild.name}\" = <{ctx.guild.id}>)")
            await ctx.send("The bot is already correctly running on this server")

        # Checks Perms
        if self.bot.has_required_perms(ctx.guild):

            # Start Task
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"Starting bg Task for <{ctx.guild.id}>")
            self.bot.bg_task.append(self.bot.loop.create_task(self.bot.random_play(ctx.guild)))
            await ctx.send("Succesfully started the bot")

        # Send error Message
        else:
            await self.bot.get_bot_channel(str(ctx.guild.id))[1].send(f"The Bot apparantly doesn't have all "
                                                                      f"required Permissions."
                                                                      f" This is possibly due to a new "
                                                                      f"Updade to the Bot.\n"
                                                                      f"To fix this add it through this link: "
                                                                      f"{self.bot.invite}\n"
                                                                      f"You don't have to remove it to do this!\n\n"
                                                                      f"Then run `$coco start`")
