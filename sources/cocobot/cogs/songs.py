# imports
import json
import logging

from discord.ext import commands


class Songs(commands.Cog):
    """Commands related to the Songs being played"""

    def __init__(self, bot):
        """
        :param bot: The cocobot using this Cog
        """

        commands.Cog.__init__(self)

        # Initialising Logger
        self.logger = logging.getLogger("cocobot.cogs.songs")

        # Setting reference to bot
        self.bot = bot

    @commands.command()
    async def add_song(self, ctx: commands.Context, *, arg=None):
        """
        Adds a new song to the list of playable songs
        :param ctx: The commands context
        :param arg: The commands arguments
        """

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"add_song called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                              f"with arg = \"{arg}\"")

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"add_song on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) called outside Bot Channel")
            return

        # If not used correctly, tells the user how to use it
        if arg is None:
            await ctx.send("Usage: `$coco add_song <Youtube link>`")
            return

        # Denies non Admins access to this command
        if not ctx.author.guild_permissions.administrator:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"add_song called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                                  f"by non admin")
            await ctx.send("Only Admins can add songs to the List")
            return

        # connects wavelink
        if len(self.bot.wavelink.nodes) == 0:
            await self.bot.connect_wavelink()

        # Searches for the track given by the user
        tracks = await self.bot.wavelink.get_tracks(arg)

        # Tells the user if nothing could be found
        if tracks is None:
            await ctx.send("Could not find anything on youtube for " + arg + "")
            return

        # Checks if that song is already in the list and if so tells the user
        uri = tracks[0].uri
        for t in self.bot.settings[str(ctx.guild.id)]["song"]:
            if t == uri:
                await ctx.send("That song is already in the song list")
                return

        # Adds the song to the list
        self.bot.settings[str(ctx.guild.id)]["song"].append(uri)
        with open("../resources/server_settings.json", "w") as settings:
            json.dump(self.bot.settings, settings, indent=4)
        self.logger.info(f"added \"{uri}\" "
                         f"on (\"{ctx.guild.name}\" = <{ctx.guild.id}>)")
        await ctx.send(f"Added {uri} to the list")

    @commands.command()
    async def list_songs(self, ctx):
        """
        Shows the songs in the list
        :param ctx: The Commands context
        """

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"list_songs called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>)")

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"list_songs on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) called outside Bot Channel")
            return

        # If no songs are in the list, tell the user
        if len(self.bot.settings[str(ctx.guild.id)]["song"]) == 0:
            await ctx.send("There are no songs in the list. Add one using `$coco add_song`")
            return

        # Builds the String containing the list of songs
        i = 0
        msg = ""
        for song in self.bot.settings[str(ctx.guild.id)]["song"]:
            i += 1
            msg += f"({i}):    <{song}>\n"

        # Sends the list of songs
        await ctx.send(msg)
        self.logger.info(f"listed songs on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                         f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>)")

    @commands.command()
    async def remove_song(self, ctx, arg=None):
        """
        Removes the song with the given id from the list
        :param ctx: The Context of the command
        :param arg: The given Argument
        """

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"remove_song called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                              f"with arg = \"{arg}\"")

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"remove_song on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"called outside Bot Channel")
            return

        # If not used correctly, tells the user how to use it
        if arg is None:
            await ctx.send("Usage: `$coco remove_song <Song ID>`\n"
                           "Get the song ID with `$coco list_songs`")
            return

        # Denies non Admins access to this command
        if not ctx.author.guild_permissions.administrator:
            await ctx.send("Only Admins can remove songs from the List")
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"remove_song called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                                  f"by non admin")
            return

        # converts the argument into an int. If not convertible tells the user to use an int
        try:
            song_id = int(arg)
        except ValueError:
            await ctx.send(f"This command only works with IDs. {arg} "
                           f"is not an ID. To find the ID of a song you want to remove use `$coco list_songs`")
            return

        # If the ID is not in the list, tell the user so
        if len(self.bot.settings[str(ctx.guild.id)]["song"]) < song_id or song_id <= 0:
            await ctx.send(f"There is no song with the ID {song_id}")
            return

        # Remove the song
        uri = self.bot.settings[str(ctx.guild.id)]["song"][song_id - 1]
        del(self.bot.settings[str(ctx.guild.id)]["song"][song_id - 1])
        with open("../resources/server_settings.json", "w") as settings:
            json.dump(self.bot.settings, settings, indent=4)
        self.logger.info(f"removed \"{uri}\" "
                         f"from (\"{ctx.guild.name}\" = <{ctx.guild.id}>)")
        await ctx.send(f"Removed {uri} from the list")
