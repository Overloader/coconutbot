# imports
import discord
import json
import logging

from discord.ext import commands


class Channel(commands.Cog):
    """Groups commands related to channels"""

    def __init__(self, bot):
        """
        :param bot: The Bot using this Cog
        """

        commands.Cog.__init__(self)

        # Initialising logger
        self.logger = logging.getLogger("cocobot.cogs.channels")

        # Setting reference to bot
        self.bot = bot

    @commands.command()
    async def add_channel(self, ctx, *, arg=None):
        """
        Adds a channel to the list of channels the cocobot can join
        :param ctx: The Context of the Command
        :param arg: The argument of the command
        """

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"add_channel called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                              f"with arg = \"{arg}\"")

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"add channel on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"called outside Bot Channel")
            return

        # Denies non Admins access to this command
        if not ctx.author.guild_permissions.administrator:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"add_channel called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                                  f"by non admin")
            await ctx.send("Only Admins can add songs to the List")
            return

        # If not used correctly, tells the user how to use it
        if arg is None:
            await ctx.send("Usage: `$coco add_channel <Channel Name>`")
            return

        # Search for the channel object matching the given name
        channel = None
        for channel in ctx.guild.channels:
            if isinstance(channel, discord.VoiceChannel) and channel.name == arg:
                break
            channel = None

        # If no channel is found, tell the user
        if channel is None:
            await ctx.send(f"No voice-channel named \"{arg}\" found")
            return

        # If the channel is already in the list, tell the user
        for channel_id in self.bot.settings[str(ctx.guild.id)]["channels"]:
            if channel_id == channel.id:
                await ctx.send(f"\"{arg}\" is already in the channel list")
                return

        # Add channel to the list
        self.bot.settings[str(ctx.guild.id)]["channels"].append(channel.id)
        with open("../resources/server_settings.json", "w") as settings:
            json.dump(self.bot.settings, settings, indent=4)
        self.logger.info(f"added (\"{channel.name}\" = <{channel.id}>) "
                         f"on (\"{ctx.guild.name}\" = <{ctx.guild.id}>)")
        await ctx.send(f"Added {arg} to the list")

    @commands.command()
    async def list_channels(self, ctx):
        """
        Lists all channels the cocobot can join
        :param ctx: The context of the Command
        """

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"list_channels called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>)")

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"list_channels on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"called outside Bot Channel")
            return

        # If the list is empty, tell the user
        if len(self.bot.settings[str(ctx.guild.id)]["channels"]) == 0:
            await ctx.send("The channel list is empty. You can add channels using `$coco add_channel`")
            return

        # build the string listing the channels
        msg = ""
        i = 0
        for channel_id in self.bot.settings[str(ctx.guild.id)]["channels"]:
            i += 1
            msg += f"({i}):    {ctx.guild.get_channel(channel_id).name}\n"

        # send the list
        await ctx.send(msg)
        self.logger.info(f"listed channels on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                         f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>)")

    @commands.command()
    async def remove_channel(self, ctx, arg=None):
        """
        Removes the channel with the given id from the list
        :param ctx: The Context of the command
        :param arg: The given Argument
        """

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"remove_channel called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                              f"with arg = \"{arg}\"")

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"remove_channel on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"called outside Bot Channel")
            return

        # If not used correctly, tells the user how to use it
        if arg is None:
            await ctx.send("Usage: `$coco remove_channel <Channel ID>`\n"
                           "Get the channel ID with `$coco list_channels`")
            return

        # Denies non Admins access to this command
        if not ctx.author.guild_permissions.administrator:
            await ctx.send("Only Admins can remove channels from the List")
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"remove_channel called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                                  f"by non admin")
            return

        # converts the argument into an int. If not convertible tells the user to use an int
        try:
            channel_id = int(arg)
        except ValueError:
            await ctx.send(f"This command only works with IDs. {arg} "
                           f"is not an ID. To find the ID of a channel you want to remove use `$coco list_channels`")
            return

        # If the ID is not in the list, tell the user so
        if len(self.bot.settings[str(ctx.guild.id)]["channels"]) < channel_id or channel_id <= 0:
            await ctx.send(f"There is no channel with the ID {channel_id}")
            return

        # Remove the channel
        channel = self.bot.settings[str(ctx.guild.id)]["channels"][channel_id - 1]
        del (self.bot.settings[str(ctx.guild.id)]["channels"][channel_id - 1])
        with open("../resources/server_settings.json", "w") as settings:
            json.dump(self.bot.settings, settings, indent=4)
        self.logger.info(f"removed (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                         f"from (\"{ctx.guild.name}\" = <{ctx.guild.id}>)")
        await ctx.send(f"Removed {ctx.guild.get_channel(channel).name} from the list")

    @commands.command()
    async def set_bot_channel(self, ctx, *, arg=None):
        """
        Sets the Bots Text Channel
        :param ctx: The context of the command
        :param arg: The commands arguments
        """

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(f"set_bot_channel called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                              f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                              f"with arg = \"{arg}\"")

        # ignore if not sent in bot channel
        bot_channel_result = self.bot.get_bot_channel(str(ctx.guild.id))
        if bot_channel_result[0] and bot_channel_result[1].id != ctx.channel.id:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"set_bot_channel on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"called outside Bot Channel")
            return

        # Denies non Admins access to this command
        if not ctx.author.guild_permissions.administrator:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"add_channel called on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                                  f"in (\"{ctx.channel.name}\" = <{ctx.channel.id}>) "
                                  f"by non admin")
            await ctx.send("Only Admins can add set the Bot Channel")
            return

        # If not used correctly, tells the user how to use it
        if arg is None:
            await ctx.send("Usage: `$coco set_bot_channel <Channel Name>`")
            return

        # Search for the channel object matching the given name
        channel = None
        for channel in ctx.guild.text_channels:
            if channel.name == arg:
                break
            channel = None

        # If no channel is found, tell the user
        if channel is None:
            await ctx.send(f"No text-channel named \"{arg}\" found")
            return

        # Add channel to the list
        self.bot.settings[str(ctx.guild.id)]["Bot Channel"] = channel.id
        with open("../resources/server_settings.json", "w") as settings:
            json.dump(self.bot.settings, settings, indent=4)
        self.logger.info(f"set (\"{channel.name}\" = <{channel.id}>) "
                         f"on (\"{ctx.guild.name}\" = <{ctx.guild.id}>) "
                         f"as Bot Channel")
        await ctx.send(f"Set {arg} as the Bots Textchannel. From now on it will only react to commands in that channel")
