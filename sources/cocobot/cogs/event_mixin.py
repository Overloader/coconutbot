import logging
import wavelink
import time
from discord.ext import commands


class EventMixin(commands.Cog, wavelink.meta.WavelinkMixin):
    """
    The class handling Wavelinks event callbacks
    """

    # The logger
    logger = logging.getLogger("cocobot.cogs.EventMixin")

    def __init__(self, bot):
        """
        Initialises the Mixin
        :param bot: The Bot that owns the cog
        """

        super(commands.Cog, self).__init__()
        super(wavelink.meta.WavelinkMixin, self).__init__()

        self.bot = bot

    @wavelink.meta.WavelinkMixin.listener()
    async def on_track_exception(self, node: wavelink.Node, payload: wavelink.events.TrackException):
        """
        Handling a TrackException
        :param node: The associated node
        :param payload: The caused exception
        """

        self.logger.exception(f"Trackexception in {payload.player.guil_id}: {payload.error}")

    @wavelink.meta.WavelinkMixin.listener()
    async def on_track_end(self, node: wavelink.Node, payload: wavelink.events.TrackEnd):
        """
        Disconnecting the Player after the track ends end resuming the task
        :param node: The asociated node
        :param payload: The TrackEnd event
        """

        # Wait 1 second so the short tracks finish playing
        time.sleep(1)
        # Disconnects Player
        await payload.player.disconnect()

        # Starts new random_play
        self.bot.bg_task.append(self.bot.loop.create_task(self.bot.random_play(
            self.bot.get_guild(payload.player.guild_id))))
