# Imports
import asyncio
import discord
import logging
import json
import random
import wavelink

from cocobot import cogs
from discord.ext import commands

# The bots intents
intents = discord.Intents.default()
intents.voice_states = True


class Bot(commands.Bot):
    """
    Inherits most from :class:`discord.ext.commands.Bot` so for Information please refer to that
    """

    invite = "https://discord.com/api/oauth2/authorize?client_id=772081351137558528&permissions=11537408&scope=bot"
    running = []

    def __init__(self, **options):
        super().__init__("$coco ", **options, intents=intents, help_command=None)

        # Initialise logger
        self.logger = logging.getLogger("cocobot.bot")

        # Loads the settings on startup
        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug("loading serversettings.json")
        with open("../resources/server_settings.json", "r") as settings:
            self.settings = json.load(settings)

        # Initialise wavelink
        self.wavelink = wavelink.Client(bot=self)

        # Add cogs
        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug("Adding Cogs")
        self.add_cog(cogs.Songs(self))
        self.add_cog(cogs.Channel(self))
        self.add_cog(cogs.Intervals(self))
        self.add_cog(cogs.Help(self))
        self.add_cog(cogs.EventMixin(self))

        # Initialise bg tasks
        self.bg_task = []

    async def on_ready(self):
        """Executes code once the Bot is ready"""

        # Sets the bots activity so users know how the helo command works
        await self.change_presence(activity=discord.Game(name="$coco help"))

        # Remove settings of servers it was removed from while offline
        delkeys = []
        for g_id in self.settings.keys():
            if self.get_guild(int(g_id)) is None:
                self.logger.info(f"Removing offline guld {g_id}")
                delkeys.append(g_id)
        for k in delkeys:
            del self.settings[k]

        # populate settings with servers that were added while the cocobot was offline
        g: discord.Guild
        for g in self.guilds:
            if not str(g.id) in self.settings.keys():
                self.logger.info(f"Adding offline guild (\"{g.name}\" = <{g.id}>)")
                self.settings[str(g.id)] = {"channels": [], "song": [], "interval": {"min": 10, "max": 120}}

        # Save settings to the JSON
        with open("../resources/server_settings.json", "w") as settings:
            json.dump(self.settings, settings, indent=4)

        # logging the bots guilds
        self.logger.info("Bot logged into following guilds:")
        for g in self.guilds:
            self.logger.info(f"(\"{g.name}\" = <{g.id}>)")

        # add tasks to the bots bg tasks
        for guild_id in self.settings.keys():

            # Checks Perms
            if self.has_required_perms(self.get_guild(int(guild_id))):

                # Start Task
                if self.logger.isEnabledFor(logging.DEBUG):
                    self.logger.debug(f"Starting bg Task for (\"{self.get_guild(int(guild_id)).name}\" = <{guild_id}>)")
                self.bg_task.append(self.loop.create_task(self.random_play(self.get_guild(int(guild_id)))))

            # Send error Message
            else:
                try:
                    await self.get_bot_channel(guild_id)[1].send(f"The Bot apparantly doesn't have "
                                                                 f"all required Permissions."
                                                                 f" This is possibly due to a new Updade to the Bot.\n"
                                                                 f"To fix this add it through this link: "
                                                                 f"<{self.invite}>\n"
                                                                 f"You don't have to remove it to do this!\n\n"
                                                                 f"Then run `$coco start`\n\n"
                                                                 f"If you need more help join the official server"
                                                                 f"https://discord.gg/C8dthmDwfn")
                except:
                    self.logger.error(f"Unexpected error when trying to send message to "
                                      f"(\"{self.get_guild(int(guild_id)).name}\" = <{guild_id}>)",
                                      exc_info=True)

    async def on_guild_join(self, guild):
        """
        Executes when joining a new Guild. Initialises the default settings

        :param guild: The :class:`discord.Guild` the cocobot just joined
        """

        self.logger.info(f"Joined new guild (\"{guild.name}\" = <{guild.id}>)")
        self.settings[str(guild.id)] = {"channels": [], "song": [], "interval": {"min": 10, "max": 120}}
        with open("../resources/server_settings.json", "w") as settings:
            json.dump(self.settings, settings, indent=4)

        # Checks Perms
        if self.has_required_perms(guild):

            # Start Task
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug(f"Starting bg Task for (\"{guild.name}\" = <{guild.id}>)")
            self.bg_task.append(self.loop.create_task(self.random_play(guild)))

        # Send error Message
        else:
            try:
                await self.get_bot_channel(str(guild.id))[1].send(f"The Bot apparantly doesn't have all "
                                                                  f"required Permissions."
                                                                  f" This is possibly due to a new Updade to the Bot.\n"
                                                                  f"To fix this add it through this link: "
                                                                  f"{self.invite}\n"
                                                                  f"You don't have to remove it to do this!\n\n"
                                                                  f"Then run `$coco start`\n\n"
                                                                  f"If you need more help join the official server"
                                                                  f"https://discord.gg/C8dthmDwfn")
            except:
                self.logger.error(f"Unexpected error when trying to send message to "
                                  f"(\"{guild.name}\" = <{guild.id}>)",
                                  exc_info=True)

    async def on_guild_remove(self, guild):
        """
        Gets executed if the Bot leaves the guild for whatever reason

        Deletes the guilds settings
        :param guild: The removed guild
        """

        self.logger.info(f"Bot removed from guild (\"{guild.name}\" = <{guild.id}>)")
        del self.settings[str(guild.id)]
        with open("../resources/server_settings.json", "w") as settings:
            json.dump(self.settings, settings, indent=4)

    async def connect_wavelink(self):
        """Connects wavelink to a node"""

        await self.wavelink.initiate_node(host='0.0.0.0',
                                          port=2333,
                                          rest_uri='http://0.0.0.0:2333',
                                          password='youshallnotpass',
                                          identifier='cocotest',
                                          region='europe')
        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug("Connected to Wavelink Node")

    async def random_play(self, guild: discord.Guild):
        """
        The method handling the random playing

        Makes the cocobot join the voice channel in random intervals and play the mp3, then leave
        """

        self.cleanup_bg_task()

        # fetches the guild by the given guild_id
        self.running.append(guild.id)

        # Run until the cocobot Application gets closed or the cocobot gets removed from the server
        if not self.is_closed() and str(guild.id) in self.settings:

            # Waits for a random amount of time between min and max interval
            interval = random.randint(self.settings[str(guild.id)]["interval"]["min"],
                                      self.settings[str(guild.id)]["interval"]["max"])
            self.logger.info(f"Waiting {interval} Minutes on (\"{guild.name}\" = <{guild.id}>)")
            await asyncio.sleep(interval * 60)

            # Checks if the cocobot is still on the server
            if str(guild.id) in self.settings.keys():

                # if no channels are configured in settings choose a random voice channel
                channel_amount = len(self.settings[str(guild.id)]["channels"])
                if channel_amount < 1:
                    if self.logger.isEnabledFor(logging.DEBUG):
                        self.logger.debug(f"(\"{guild.name}\" = <{guild.id}>) has no configured channels")
                    channels = []
                    for channel in guild.channels:
                        if isinstance(channel, discord.VoiceChannel):
                            channels.append(channel)
                    if len(channels) > 0:
                        voice_channel = channels[random.randint(1, len(channels)) - 1]
                    else:
                        voice_channel = None

                # If exactly 1 channel is configured use that one
                else:
                    if channel_amount == 1:
                        if self.logger.isEnabledFor(logging.DEBUG):
                            self.logger.debug(f"(\"{guild.name}\" = <{guild.id}>) has 1 configured channel")
                        channel_id = self.settings[str(guild.id)]["channels"][0]

                    # If more than one channel is configured join one of them at random
                    else:
                        if self.logger.isEnabledFor(logging.DEBUG):
                            self.logger.debug(f"(\"{guild.name}\" = <{guild.id}>) has {channel_amount} "
                                              f"configured channels")
                        channel_id = self.settings[str(guild.id)]["channels"][random.randint(1, channel_amount) - 1]

                    voice_channel = guild.get_channel(channel_id)

                    # Deletes channel in settings if it was deleted on the server
                    if voice_channel is None:
                        self.logger.info(f"Channel <{channel_id}> was deleted on (\"{guild.name}\" = <{guild.id}>)")
                        self.settings[str(guild.id)]["channels"].remove(channel_id)
                        with open("../resources/server_settings.json", "w") as settings:
                            json.dump(self.settings, settings, indent=4)

                # Only joins, if Members are in the voice channel
                if voice_channel is not None and len(voice_channel.members) > 0:

                    # connects wavelink
                    if len(self.wavelink.nodes) == 0:
                        await self.connect_wavelink()

                    # Decides what track to play
                    song_amount = len(self.settings[str(guild.id)]["song"])
                    if song_amount == 0:
                        if self.logger.isEnabledFor(logging.DEBUG):
                            self.logger.debug(f"(\"{guild.name}\" = <{guild.id}>) has no configured songs")
                        uri = "https://www.youtube.com/watch?v=w0AOGeqOnFY"
                    elif song_amount == 1:
                        if self.logger.isEnabledFor(logging.DEBUG):
                            self.logger.debug(f"(\"{guild.name}\" = <{guild.id}>) has 1 configured song")
                        uri = self.settings[str(guild.id)]["song"][0]
                    else:
                        if self.logger.isEnabledFor(logging.DEBUG):
                            self.logger.debug(f"(\"{guild.name}\" = <{guild.id}>) has {song_amount} configured songs")
                        uri = self.settings[str(guild.id)]["song"][random.randint(1, song_amount) - 1]
                    # Searches the Track
                    track = (await self.wavelink.get_tracks(uri))[0]

                    # Connects the player to the Channel
                    self.logger.info(f"Connecting player on (\"{guild.name}\" = <{guild.id}>) to "
                                     f"(\"{voice_channel.name}\" = <{voice_channel.id}>) playing {uri}")
                    player = self.wavelink.get_player(guild.id)
                    await player.connect(voice_channel.id)

                    # deafen bot
                    await guild.get_member(self.user.id).edit(deafen=True)

                    # Start playing
                    await player.play(track)

                else:
                    self.bg_task.append(self.loop.create_task(self.random_play(guild)))

    def get_bot_channel(self, guild_id: str) -> (bool, discord.TextChannel):
        """
        Returns the bots channel in the specified guild
        :param guild_id: The id of the guild to search the channel of
        :return: The TextChannel the bot should use
        """

        # If Bot Channel is defined, return it
        if "Bot Channel" in self.settings[guild_id].keys():
            return True, self.get_guild(int(guild_id)).get_channel(self.settings[guild_id]["Bot Channel"])

        # If Bot Channel is not defined, return first text channel the bot can send in
        else:
            self.logger.warning(f"No Bot channel defined for ({self.get_guild(int(guild_id)).name} = <{guild_id}>)")
            channel: discord.TextChannel
            for channel in self.get_guild(int(guild_id)).text_channels:
                if channel.permissions_for(self.get_guild(int(guild_id)).get_member(self.user.id)).send_messages:
                    return False, channel

    def has_required_perms(self, guild: discord.Guild) -> bool:
        """
        Checks if the bot has all required permissions on the given guild
        :param guild: The guild to check
        :return: If the bot has the permissions
        """

        perms: discord.Permissions = guild.get_member(self.user.id).guild_permissions
        if perms.connect and perms.deafen_members and perms.read_messages and perms.speak and perms.view_channel:
            return True
        else:
            self.logger.warning(f"Bot doesnt have all required permissions on"
                                f"(\"{guild.name}\" = <{guild.id}>)")
            return False

    def cleanup_bg_task(self):
        """
        Deletes all finished tasks from bg_task
        """

        # Collects all finished bg tasks
        done = []
        for task in self.bg_task:
            if task.done():
                done.append(task)

        self.logger.info(f"Cleaning up {len(done)} bg_tasks")

        # remove finished tasks from bg_task
        for task in done:
            self.bg_task.remove(task)
